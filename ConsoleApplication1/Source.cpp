#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <string>
#include <utility>
#include <boost/filesystem.hpp>

using namespace boost::filesystem;

using namespace cv;
using namespace std;

int getSum(int i, int j, Mat img);

Mat blurImage;
int mask[5][5] = {
	{ 1, 4, 6, 4, 1 } ,
	{ 4, 16, 24, 16, 4 } ,
	{ 6, 24, 36, 24, 6 },
	{ 4, 16, 24, 16, 4 } ,
	{ 1, 4, 6, 4, 1 }
};

int main(int argc, char **argv) {
	
	// Grab first command line parameter as path
	path p(argv[1]);
	path p2(argv[2]);
	
	if (argc < 3) {
		cout << "Need more params \n";
	}



	// Create "end-of-list" iterator
	// (basically, the default constructor gives you what will
	// be returned by the iterator when there are no more elements left)
	directory_iterator end_itr;

	// Create directory iterator for this path, and then
	// loop through everything in directory
	for (directory_iterator itr(p); itr != end_itr; ++itr) {

		// Print path to file
		//cout << itr->path().parent_path().string() << "/";
		
		// Print filename
		//cout << itr->path().filename().string() << " ";

		// Print extension
		//cout << itr->path().extension().string() << " ";
		
		String ext = itr->path().extension().string();
		
		if (is_regular_file(itr->status()) && (ext == ".bmp" || ext ==".BMP")) {
		
			Mat image;
			image = imread(itr->path().string(), 0);
			if (!image.data) {
				cout << "ERROR: Could not open or find the image!" << endl;
				return -1;
			}
			//imshow("Our image", image);
			uchar pixel = image.at <uchar >(18, 18);
			
			blurImage = Mat::zeros(cv::Size(image.cols, image.rows), CV_8UC1);
			for (int i = 0; i < image.rows; i++) {
				for (int j = 0; j < image.cols; j++) {
				//apply condition here
					
					try {
						blurImage.at<uchar>(i, j) = getSum(i, j, image) / 256;
					}
					catch (cv::Exception& e) {
						const char* err_msg = e.what();
						cout << "ERROR "   << err_msg << "\n";
					}
   				}
			}

			try {
				if (boost::filesystem::create_directory(p2))
				{						
					imwrite(p2.string() + "\\" + itr->path().filename().string(), blurImage);				
					
				}
				else {					
					imwrite(p2.string() + "\\" + itr->path().filename().string(), blurImage);
				
				}
								
			}
			catch (runtime_error& ex) {
				fprintf(stderr, "Exception converting image to PNG format: %s\n", ex.what());
				return 1;
			}
			blurImage = NULL;
			waitKey(-1);

		}
	
	}
	
	destroyAllWindows();

	return 0;
}

int getSum (int row, int col, Mat img){
	int sum = 0;
	int i, j, t=0;
	int counter = 0;
	i = row - 2;
	for (int n = 0; n < 5; n++) {
		j = col - 2;
		for (int m = 0; m < 5; m++) {
			
			if (((i<0) || (i >= img.rows) )||((j<0) ||( j >= img.cols))) {
				sum=sum+( 128*mask[n][m]);
			}
			else {
				try {
					t = img.at<uchar>(i, j);
				}
				catch (cv::Exception& e) {
					const char* err_msg = e.what();
					cout << " Error " << err_msg << "\n";					
				}
				sum= sum + (t*mask[n][m]);
			}	
			counter += 1;
			j++;
		}
		i++;
		
	}
	return sum;
}
