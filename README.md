# **Blurring image with Gaussian filter. Using OpenCV to convert img to matrix.** #
**
Version 1.0**

### Takes 2 arguments 
    1.  Folder path that contains images (.bmp, .BMP)
    2. Output folder path where blurred images will be placed